package org.example;

import org.example.model.*;
import org.example.utils.SessionFactoryMaker;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.repository.ClientRepository;
import org.repository.OrderRepository;
import org.repository.ProductRepository;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        OrderRepository orderRepository = new OrderRepository(SessionFactoryMaker.getFactory().openSession());
        ClientRepository clientRepository = new ClientRepository(SessionFactoryMaker.getFactory().openSession());
        ProductRepository productRepository = new ProductRepository(SessionFactoryMaker.getFactory().openSession());

        Client client = new Client("Ioana", "Emilia");
//        Client client1 = new Client("Timis", "Mihai");
        Product product = new Product("jeans", 12d, Category.CLOTHES.CLOTHES, ProductType.DRESSES);
        Product product2 = new Product("tricou", 12d, Category.JEWELLERY, ProductType.DRESSES);
        Product product3 = new Product("tenesi", 12d, Category.CLOTHES, ProductType.SHOES);
        Product product4 = new Product("inel", 20d, Category.JEWELLERY, ProductType.BRACELETS);
        Product product5 = new Product("cercei", 30d, Category.JEWELLERY, ProductType.EARRINGS);
        Product product6 = new Product("rochie", 50d, Category.CLOTHES, ProductType.DRESSES);
        Order order2 = new Order(23d, LocalDateTime.now(), "Vasile Lupu", client);
//        order2.addProduct(product);
//        order2.addProduct(product2);
//        order2.addProduct(product3);
//
//        order2.setClient(client);
//        order2.setClient(client1);

/*        order2.addProduct(session.get(Product.class,1l));
        order2.addProduct(session.get(Product.class,2l));
        order2.addProduct(session.get(Product.class,3l));*/

//
//        Order order = orderRepository.findById(1L);
//
//        orderRepository.read();
//
//        orderRepository.delete(1l);
//
//        System.out.println(order.toString());

        productRepository.update(product);
        productRepository.update(product2);
        productRepository.update(product3);
        productRepository.update(product4);
        productRepository.update(product5);
        productRepository.update(product6);

        order2.addProduct(product);
        order2.addProduct(product2);
        order2.addProduct(product3);
        order2.addProduct(product4);
        order2.addProduct(product5);
        order2.addProduct(product6);

        order2.setClient(client);

        List<Product> productList = new ArrayList<>();
        List<Product> cosCumparaturi = new ArrayList<>();
        cosCumparaturi = productRepository.read();

        List<Order> orderList = orderRepository.read();
//        orderRepository.update(order2);


        Scanner keyboard = new Scanner(System.in);

        System.out.println("Tastati cifra corespunzatoare comenzii: ");
        System.out.println("1 pentru Produse ");
        System.out.println("2 pentru Comenzi ");
        System.out.println("3 pentru Clienti ");

        while (keyboard.hasNext()) {
            int selectTabel = Integer.valueOf(keyboard.nextLine());

            if (selectTabel == 1) {
                System.out.println("Tastati cifra corespunzatoare actiunii dorite din categoria produse");
                System.out.println("1 pentru adaugare");
                System.out.println("2 pentru stergere");
                System.out.println("3 pentru afisare");

                int selectProduct = Integer.valueOf(keyboard.nextLine());
                if (selectProduct == 1) {
                    System.out.println("Aceasta este lista de produse: ");
                    productList = productRepository.read();
                    productList.stream().forEach(System.out::println);
                    long selectId = -1;
                    while (selectId != 0) {
                        System.out.println("Adaugati ID-ul produsului dorit in cosul de cumparaturi");
                        selectId = Integer.valueOf(keyboard.nextLine());
                        cosCumparaturi.add(productRepository.findById(selectId));
                        System.out.println(cosCumparaturi);
                    }
                }
                if (selectProduct == 2) {
                    System.out.println("Scrieti ID-ul produsului pe care doriti sa il stergeti");
                    System.out.println("Aceasta este cosul de cumparaturi: ");
                    cosCumparaturi.forEach(System.out::println);
                    long selectId = Integer.valueOf(keyboard.nextLine());
                    selectId = -1;
                    while (selectId != 0) {
                        System.out.println("Stergeti ID-ul produsului pe care doriti sa-l stergeti din cosul de cumparaturi");
                        selectId = Integer.valueOf(keyboard.nextLine());
                        cosCumparaturi.remove(productRepository.findById(selectId));
                        System.out.println(cosCumparaturi);
                    }
                }
                if (selectProduct == 3) {
                    System.out.println("Aceasta este cosul de cumparaturi: ");
                    cosCumparaturi.forEach(System.out::println);
                }
            }

//            if (selectTabel == 2) {
//                System.out.println("Tastati cifra corespunzatoare actiunii dorite din comanda");
//                System.out.println("1 pentru adaugare");
//                System.out.println("2 pentru afisare");
//
//                int selectProduct = Integer.valueOf(keyboard.nextLine());
//                if (selectProduct == 1) {
//                    System.out.println("Aceasta este lista de comenzi: ");
//                    orderList.stream().forEach(System.out::println);
//                    long selectClientId = -1;
//                    while (selectClientId != 0) {
//                        System.out.println("Adaugati ID-ul dumneavoastra de client");
//                        selectClientId = Integer.valueOf(keyboard.nextLine());
//                        orderList.add(orderRepository.findById(selectClientId));
//                        System.out.println(orderList);
//                    }
//                }
//            }
        }


    }
}