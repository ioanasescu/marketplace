package org.example.model;


import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long idOrder;

    @Column(name = "total_price")
    private double totalPrice = 0d;
    @Column(name = "subsmission_date")
    private LocalDateTime dateOfSubmission;

    @ManyToMany (cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "register",
            joinColumns = @JoinColumn (name = "id_Order"),
            inverseJoinColumns = @JoinColumn (name = "id_Product")
    )
    private Set<Product> products = new HashSet<>();

    @ManyToOne (cascade = CascadeType.PERSIST)
    @JoinColumn(name = "client_id")
    private Client client;

    @Column(name = "delivery_address")
    private String deliveryAddress;

    public void addProduct(Product product){
        this.products.add(product);
        product.getOrders_().add(this);
        this.setTotalPrice(products.stream().mapToDouble(Product::getPrice).sum());
    }

    public Order(double totalPrice, LocalDateTime dateOfSubmission, String deliveryAddress, Client client) {
        this.totalPrice = totalPrice;
        this.dateOfSubmission = dateOfSubmission;
        this.deliveryAddress = deliveryAddress;
        this.client = client;
    }

    @Override
    public String toString() {
        return "Order{" +
                "idOrder=" + idOrder +
                ", totalPrice=" + totalPrice +
                ", dateOfSubmission=" + dateOfSubmission +
                ", deliveryAddress='" + deliveryAddress + '\'' +
                '}';
    }

    public Long getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Long idOrder) {
        this.idOrder = idOrder;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public LocalDateTime getDateOfSubmission() {
        return dateOfSubmission;
    }

    public void setDateOfSubmission(LocalDateTime dateOfSubmission) {
        this.dateOfSubmission = dateOfSubmission;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

}
