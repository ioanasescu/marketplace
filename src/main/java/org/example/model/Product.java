package org.example.model;


import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long idProduct;
    private String title;
    private Double price;
    @Enumerated (EnumType.STRING)
    private Category category;

    @Enumerated (EnumType.STRING)
    private ProductType productType;

    @ManyToMany(mappedBy = "products")
    private Set<Order> orders_ = new HashSet<>();

    public Product(String title, Double price, Category category, ProductType productType) {
        this.title = title;
        this.price = price;
        this.category = category;
        this.productType = productType;
    }

    @Override
    public String toString() {
        return "Product{" +
                "idProduct=" + idProduct +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", category=" + category +
                ", productType=" + productType +
                ", orders_=" + orders_ +
                '}';
    }
}
