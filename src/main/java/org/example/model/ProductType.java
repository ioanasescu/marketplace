package org.example.model;

import lombok.Getter;

import javax.persistence.*;


public enum ProductType {
    DRESSES, SHOES, JEANS, EARRINGS, BRACELETS, RINGS;
}
