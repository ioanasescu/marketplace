package org.example.utils;

import org.example.model.Category;
import org.example.model.Order;
import org.example.model.Product;
import org.example.model.ProductType;

import java.util.List;
import java.util.Scanner;

public class ScannerUtils {

    private static Scanner keyboard;

    public static Scanner getScanner() {
        if (keyboard == null) {
            keyboard = new Scanner(System.in);
        }
        return keyboard;
    }
/*
    public static Product addProducts(Scanner productType){
        System.out.println("Adaugati produsul dorit");
       productType = getScanner();


        return ;
    }*/
}
