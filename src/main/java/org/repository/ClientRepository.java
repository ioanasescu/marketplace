package org.repository;

import lombok.AllArgsConstructor;
import org.example.model.Client;
import org.example.model.Order;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

@AllArgsConstructor
public class ClientRepository implements Repository<Client>{
    private final EntityManager entityManager;

    @Override
    public List<Client> read() {
        return entityManager.createQuery("select client from Client client").getResultList();
    }

    @Override
    public void update(Client client) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(client);
        transaction.commit();
    }

    @Override
    public void delete(Long id) {
        EntityTransaction transaction = entityManager.getTransaction();
        Client client = entityManager.find(Client.class, id);
        transaction.begin();
        entityManager.remove(client);
        transaction.commit();
    }

    @Override
    public void save(Client client) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(client);
        transaction.commit();
    }

    @Override
    public Client findById(Long id) {
        return  entityManager.find(Client.class, id);
    }
}
