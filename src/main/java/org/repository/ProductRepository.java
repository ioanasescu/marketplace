package org.repository;

import lombok.AllArgsConstructor;
import org.example.model.Client;
import org.example.model.Product;
import org.example.model.ProductType;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

@AllArgsConstructor
public class ProductRepository implements Repository<Product>{
    private final EntityManager entityManager;

    @Override
    public List<Product> read() {
        return entityManager.createQuery("select prod from Product prod").getResultList();
    }

    @Override
    public void update(Product product) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(product);
        transaction.commit();
    }

    @Override
    public void delete(Long id) {
        EntityTransaction transaction = entityManager.getTransaction();
        Product product = entityManager.find(Product.class, id);
        transaction.begin();
        entityManager.remove(product);
        transaction.commit();
    }

    @Override
    public void save(Product product) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(product);
        transaction.commit();
    }

    @Override
    public Product findById(Long id) {
        return  entityManager.find(Product.class, id);
    }
}
