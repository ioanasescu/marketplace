package org.repository;

import lombok.AllArgsConstructor;
import org.example.model.Order;
import org.example.utils.SessionFactoryMaker;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

@AllArgsConstructor
public class OrderRepository implements Repository<Order>{

    private final EntityManager entityManager;

    @Override
    public List<Order> read() {
        return entityManager.createQuery("select ord from Order ord").getResultList();
    }

    @Override
    public void update(Order order) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(order);
        transaction.commit();
    }

    @Override
    public void delete(Long id) {
        EntityTransaction transaction = entityManager.getTransaction();
        Order order = entityManager.find(Order.class, id);
        transaction.begin();
        entityManager.remove(order);
        transaction.commit();
    }

    @Override
    public void save(Order order) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(order);
        transaction.commit();
    }

    @Override
    public Order findById(Long id) {
        return  entityManager.find(Order.class, id);
    }

}
