package org.repository;

import java.util.List;
import java.util.Optional;

public interface Repository <T> {

    List<T> read(); //createQuery !!!

    void update(T obj); // persist

    void delete(Long id);  // remove/delete?

    void save(T obj);// persist

    T findById(Long id); // find

}

